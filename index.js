var createError = require('http-errors');
const express = require('express');
const logger = require('morgan');
const debug = require('debug');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const loads = require('./src/loads');

const appDebugger = debug('monaco:debugger');
const fail = debug('monaco:falha');
var app = express();

(async () => {
  let raceFromDB = await require('./src/BLL.RaceTrack').getRacetracks();
  let newRacetrack = {};
  raceFromDB.racetracks.map(el => newRacetrack[el.name] = { length: el.length, curves: parseFloat(el.curves), region: el.region });
  loads.MEMORYSTORAGE_RACETRACK = newRacetrack;
  console.log(loads.MEMORYSTORAGE_RACETRACK);
})()

const appName = 'monaco';
appDebugger('Starting app %o', appName);

// app.use(logger('dev'));
app.use(logger('combined'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter(loads));
app.use('/users', usersRouter);

app.get('/http-error', (req, res, next) => next(createError(401, 'Teste')))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  return next(createError(404, 'Page not found', { expose: false }));
});

app.listen(3000, err => err ? fail(err.message) : appDebugger(`Server listening on port 3000`));

// setInterval(() => console.log(loads.MEMORYSTORAGE_RACETRACK), 60000)

module.exports = app;