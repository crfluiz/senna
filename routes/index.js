const express = require('express');
const router = express.Router();

const RaceTrack = require('../src/BLL.RaceTrack');
const sequelizeHealthCheck = require('../sequelize/healthCheck');

module.exports = (loads) => {
  router.get('/', async (req, res) => {
    const result = await sequelizeHealthCheck();
    return res.status(result.ok ? 200 : 400).json(result);
  });

  router.post('/create/:name', async (req, res) => {
    let rt = new RaceTrack({ name: req.params['name'], status: { length: req.body.length, curves: req.body.curves } });
    let result = rt.createInMemory(loads);
    return res.status(result.ok ? 200 : 400).json(result);
  });

  router.get('/get/memory', (req, res) => {
    res.status(200).json({ ok: true, racetracks: loads.MEMORYSTORAGE_RACETRACK });
  });

  router.get('/write/frommemory/infile/:name', async (req, res) => {
    let result = await RaceTrack.writeFromMemoryInFile(req.params['name'], loads);
    return res.status(result.ok ? 200 : 400).json(result);
  });

  router.get('/write/indb/frommemory', async (req, res) => {
    let result = await RaceTrack.writeMemoryInDB(loads);
    res.status(result.ok ? 200 : 400).json(result)
  });

  router.get('/write/indb/fromfile/:filename', async (req, res) => {
    let result = await RaceTrack.writeFileInDB(req.params['filename']);
    res.status(result.ok ? 200 : 400).json(result)
  });

  router.get('/import/inmemory/fromfile/:name', async (req, res) => {
    let result = await RaceTrack.importInMemoryFromFile(req.params['name'], loads);
    return res.status(result.ok ? 200 : 400).json(result);
  });

  router.get('/sequelize/quick-example/:name', async (req, res) => {
    let name = req.params['name'];
    await sequelize.sync();
    const person = await User.create({
      username: name,
      birthday: new Date(1980, 6, 20)
    });
    res.status(200).json(person.toJSON());
  })

  router.get('/import/inmemory/fromdb', async (req, res) => {
    let result = await RaceTrack.getRacetracks();
    if(!result.ok) return res.status(400).json(result);
    let newRacetrackMemoryStorage = {};
    result.racetracks.map(el => newRacetrackMemoryStorage[el.name] = { length: el.length, curves: parseFloat(el.curves), region: el.region });
    loads.MEMORYSTORAGE_RACETRACK = newRacetrackMemoryStorage;
    res.status(200).json({ ...result, newRacetrackMemoryStorage });
  });

  router.get('/auto-create/:type/:count/:region', async (req, res) => {
    // regions
    // 0 .. 6 - ['africa', 'centralAmerica', 'europe', 'india', 'northAmerica', 'oceania', 'southAmerica']
    // 7 - One random nationality for all racetracks 
    // 8+ - All racetracks with random nationality
    let result = await RaceTrack.autoCreate(req.params['type'], req.params['count'], req.params['region']);
    res.status(result.ok ? 200 : 400).json(result);
  });

  return router;
}