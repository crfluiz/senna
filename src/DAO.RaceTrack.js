const { RaceTrack: RaceTrackModel } = require('../sequelize/models').models;

class RaceTrack {
    static async bulkCreateRacetrack(raceTrackArray){        
        try{
            const bulkCreate = await RaceTrackModel.bulkCreate(raceTrackArray);
            return { ok: true, message: 'Racetrack Array', raceTrackArray, bulkCreate }
        }catch(error){
            console.error(error)
            return { ok: false, message: 'error', error }
        }
    }

    static async getRacetracks(){
        try{
            let racetracks = await RaceTrackModel.findAll({
                attributes: ['id', 'name', 'length', 'curves', 'region']
            });
            return { ok: true, racetracks };
        }catch(error){
            return { ok: false, error };
        }
    }
}

module.exports = RaceTrack;