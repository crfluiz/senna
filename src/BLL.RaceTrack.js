const fs = require('fs').promises;
const { Buffer } = require('buffer');
const RaceTrackDAO = require('./DAO.RaceTrack');

const listStats = {
    'default': {
        length: {
            max: 1700,
            min: 800
        },
        curves: {
            max: 0.43,
            min: 0.18
        }
    },
    'small': {
        length: {
            max: 900,
            min: 600
        },
        curves: {
            max: 0.41,
            min: 0.20
        }
    },
    'medium': {
        length: {
            max: 1500,
            min: 901
        },
        curves: {
            max: 0.43,
            min: 0.17
        }
    },
    'big': {
        length: {
            max: 2900,
            min: 1501
        },
        curves: {
            max: 0.45,
            min: 0.15
        }
    },
    'gragas': {
        length: {
            max: 6000,
            min: 3000
        },
        curves: {
            max: 0.60,
            min: 0.10
        }
    },
    'whatever_normal': {
        length: {
            max: 2900,
            min: 600
        },
        curves: {
            max: 0.45,
            min: 0.15
        }
    },
    'whatever': {
        length: {
            max: 9999,
            min: 600
        },
        curves: {
            max: 0.50,
            min: 0.10
        }
    }
}

const getRandomIntInclusive = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const createName = async (count, region) => {
    let filename = citiesRegions[region];
    let data = await fs.readFile(`/storageapp/${filename}.names`, { encoding: 'utf8' });
    let dataArray = data.split('\n');
    let raceTrackName = "";
    let i = count;
    while(i != 0){
        let choice = getRandomIntInclusive(0, dataArray.length - 1);
        raceTrackName = `${raceTrackName} ${dataArray[choice]}`;
        i -= 1;
    }
    raceTrackName = raceTrackName.trim();
    return { name: raceTrackName, region: filename };
}

const racetrackarrayToObject = async (racetrackarray) => {
    let racetrackobject = {};
    await racetrackarray.map(el => racetrackobject[el.name] = { length: el.length, curves: el.curves, region: el.region })
    return racetrackobject
}

const citiesRegions = ['africa', 'centralAmerica', 'europe', 'india', 'northAmerica', 'oceania', 'southAmerica']

class RaceTrack {
    constructor(racetrackProps){
        this.name = racetrackProps.name;
        this.status = racetrackProps.status;
    }

    static getRacetrack(name, storage){}

    createInMemory(loads){
        if(loads.MEMORYSTORAGE_RACETRACK[this.name]) return { ok: false, message: `Already exists a racetrack ${this.name}` };
        loads.MEMORYSTORAGE_RACETRACK[this.name] = { length: this.status.length, curves: this.status.curves };
        return { ok: true, message: 'Racetrack added!', racetrackName: this.name };
    }

    static async writeFromMemoryInFile(name, loads){
        let data = new Uint8Array(Buffer.from(JSON.stringify(loads.MEMORYSTORAGE_RACETRACK)));
        try {
            await fs.writeFile(`/storageapp/${name}.monaco.json`, data);
            return { ok: true, message: 'Criado' };
        }catch(error){
            return { ok: false, error };
        }
    }

    static async importInMemoryFromFile(name, loads){
        try {
            const data = await fs.readFile(`/storageapp/${name}.monaco.json`, { encoding: 'utf-8' });
            loads.MEMORYSTORAGE_RACETRACK = JSON.parse(data);
            return { ok: true, racetracks: JSON.parse(data) };
        }catch(error){
            return { ok: false, error };
        }
    }

    static async writeMemoryInDB(loads){
        const raceTrackArray = [];
        for(let name in loads.MEMORYSTORAGE_RACETRACK){
            let data = { name, length: loads.MEMORYSTORAGE_RACETRACK[name].length, curves: loads.MEMORYSTORAGE_RACETRACK[name].curves, region: loads.MEMORYSTORAGE_RACETRACK[name].region }
            raceTrackArray.push(data);
        }
        return await RaceTrackDAO.bulkCreateRacetrack(raceTrackArray);
    }

    static async writeFileInDB(filename){
        const raceTrackArray = [];
        let fileData = await fs.readFile(`/storageapp/${filename}.monaco.json`, { encoding: 'utf8' });
        fileData = JSON.parse(fileData);
        for(let name in fileData){
            let data = { name, length: fileData[name].length, curves: fileData[name].curves, region: fileData[name].region }
            raceTrackArray.push(data);
        }
        return await RaceTrackDAO.bulkCreateRacetrack(raceTrackArray);
    }

    static async getRacetracks(){
        let racetracks = await RaceTrackDAO.getRacetracks();
        console.log(racetracks);
        return racetracks.ok ? racetracks : { ...racetracks, error: racetracks.error.message };
    }

    static async autoCreate(type, count, region){
        const racetracks = [];
        let regionRandomicity = "";
        let randomRegion = null;
        switch(parseInt(region)){
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                regionRandomicity = "HARDCODED";
                break;
            case 7:
                regionRandomicity = "SIMPLE_RANDOM";
                randomRegion = getRandomIntInclusive(0, 6);
                break;
            default:
                regionRandomicity = "FULL_RANDOM";
                break;

        }
        for(let i = 0; i < count; i++){
            let countNames = getRandomIntInclusive(1,3);
            let reg = regionRandomicity == "HARDCODED" ? region :
                      regionRandomicity == "SIMPLE_RANDOM" ? randomRegion : getRandomIntInclusive(0, 6);
            let response = await createName(countNames, reg);

            let length = getRandomIntInclusive(listStats[type].length.min, listStats[type].length.max),
               curves = getRandomIntInclusive(listStats[type].curves.min * 100, listStats[type].curves.max * 100) / 100;
            racetracks.push({ name: response.name, region: response.region, length, curves });
        }
        let fileGenerated = regionRandomicity == 'HARDCODED' ? citiesRegions[region] : regionRandomicity == 'SIMPLE_RANDOM' ? citiesRegions[randomRegion] : "mix";
        fileGenerated = `/storageapp/${fileGenerated}-${Date.now()}.monaco.json`;

        let racetrackData = await racetrackarrayToObject(racetracks);
        let data = new Uint8Array(Buffer.from(JSON.stringify(racetrackData)));
        try {
            await fs.writeFile(fileGenerated, data);
            return { ok: true, racetracks, fileGenerated };
        }catch(error){
            return { ok: false, error };
        }
    }
}

module.exports = RaceTrack;