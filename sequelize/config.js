const { Sequelize, Model, DataTypes } = require('sequelize');
const sequelize = new Sequelize('postgres://exampleuser:examplepass@postgres:5432/corrida_example');
const debug = require('debug');

module.exports = {
    sequelize,
    Model,
    DataTypes
}
